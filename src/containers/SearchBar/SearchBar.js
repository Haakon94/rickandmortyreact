import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";

class SearchBar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			search: ""
		};
		this.getSearchInput = this.getSearchInput.bind(this);
		this.handleOnClick = this.handleOnClick.bind(this);
	}

	getSearchInput(e) {
		const input = e.target.value;
		this.setState({ search: input });
	}

	handleOnClick() {
		if (this.props.onChange) {
			this.props.onChange(this.state);
		}
	}
	render() {
		return (
			<div className="row">
				<div className="col-md-6">
					<input className="form-control" placeholder="Search..." onChange={this.getSearchInput} />
				</div>
				<div className="col-md-3">
					<button
						onClick={() => this.props.onClick(this.state.search)}
						className="btn btn-primary form-control"
					>
						Search
					</button>
				</div>
			</div>
		);
	}
}

export default SearchBar;
