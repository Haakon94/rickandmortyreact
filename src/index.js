import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter} from "react-router-dom";
import {Route } from "react-router-dom";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import CardsPage from './containers/CharacterCards/CardsPage/CardsPage';
import CardPage from './containers/CharacterCards/CardPage/CardPage';
import LocationsPage from './containers/Locations/LocationsPage/LocationsPage';
import LocationPage from './containers/Locations/LocationPage/LocationPage';

ReactDOM.render(<BrowserRouter><App>
    <Route exact path="/" component={CardsPage}></Route>
    <Route path="/character/:id" component={CardPage}></Route>
    <Route path="/locations" component={LocationsPage}></Route>
    <Route path="/location/:id" component={LocationPage}></Route>
    </App>
    </BrowserRouter>, document.getElementById('root'));



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
