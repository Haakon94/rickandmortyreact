import 'bootstrap/dist/css/bootstrap.min.css';
import React from "react";
import CharacterCard from "../../../components/CharacterCard/CharacterCard";
import {NavLink} from "react-router-dom";

// Stateful component
class CardPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
            name: props.match.params.name,
            character: {}
        }
    }

    getCharacterData() {
        fetch('https://rickandmortyapi.com/api/character/'+this.state.id)
        .then(response => response.json())
        .then(data => {
                this.setState({character: data})
        })
    }

    componentDidMount(){
        this.getCharacterData()
    }
    
    render() {
        let linkButtonSingleChar = null;
        linkButtonSingleChar = <NavLink to={{ pathname: '/' }} className="nav-link">Go back to all characters</NavLink>;
        let characterCard = null;
        if (this.state.character.id ) {
            characterCard = (<CharacterCard 
                key={this.state.character.id}
                {...this.state.character}
                showLinkSingleCharacter={true}></CharacterCard>
            );
        } else {
            characterCard = <div className="spinner-grow text-muted"></div>
        }
        return(
            <React.Fragment>
            <div className="d-flex justify-content-start">{linkButtonSingleChar}</div>
                    <div className="d-flex justify-content-center col-md-12" style={{paddingTop: "2em", paddingBottom: "2em"}}>
                        <h4>Single character overview</h4>
                        </div>
                    <div className="d-flex justify-content-center col-md-12">
                {characterCard}
                </div>
            </React.Fragment>
        );
    }
}

export default CardPage;
