import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import LocationCard from "../../../components/LocationCard/LocationCard";

class LocationPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: props.match.params.id,
			name: props.match.params.name,
			location: {}
		};
	}

	getLocationData() {
		fetch("https://rickandmortyapi.com/api/location/" + this.state.id)
			.then(response => response.json())
			.then(data => {
				this.setState({ location: data });
			});
	}

	componentDidMount() {
		this.getLocationData();
	}

	render() {
		let locationCard = null;
		if (this.state.location.id) {
			locationCard = (
				<LocationCard key={this.state.location.id} {...this.state.location}></LocationCard>
			);
		} else {
			locationCard = <div className="spinner-grow text-muted"></div>;
		}
		return (
			<React.Fragment>
				<div
					className="d-flex justify-content-center col-md-12"
					style={{ paddingTop: "2em", paddingBottom: "2em" }}
				>
					<h4>Single location overview</h4>
				</div>
				<div className="d-flex justify-content-center col-md-12">{locationCard}</div>
			</React.Fragment>
		);
	}
}

export default LocationPage;
