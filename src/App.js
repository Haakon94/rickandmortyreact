import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import "./App.css";
import { NavLink } from "react-router-dom";

//import CardsPage from "./containers/CharacterCards/CardsPage/CardsPage";
//import logo from './logo.svg';

class App extends React.Component {
	state = {
		isNavigationDrawerOpen: false
	};

	toggleNavigationDrawer() {
		this.setState(prev => {
			return {
				isNavigationDrawerOpen: !prev.isNavigationDrawerOpen
			};
		});
	}

	render() {
		return (
			<div className="App">
				<header>
					<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
						<NavLink to={{ pathname: "/" }} className="navbar-brand">
							ReactJS
						</NavLink>
						<button
							className="navbar-toggler"
							type="button"
							data-toggle="collapse"
							data-target="#navbarSupportedContent"
							aria-controls="navbarSupportedContent"
							aria-expanded="false"
							aria-label="Toggle navigation"
							onClick={this.toggleNavigationDrawer.bind(this)}
						>
							<span className="navbar-toggler-icon"></span>
						</button>
						<div
							className="collapse navbar-collapse"
							id="navbarSupportedContent"
							style={{ display: this.state.isNavigationDrawerOpen ? "block" : "none" }}
						>
							<ul className="navbar-nav mr-auto">
								<li className="nav-item">
									<NavLink to={{ pathname: "/" }} className="nav-link active">
										Home
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink to={{ pathname: "/locations" }} className="nav-link active">
										Locations
									</NavLink>
								</li>
							</ul>
						</div>
					</nav>
					<div>
						<img
							src="https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720"
							className="img-fluid"
							alt="Header"
							style={{ width: "100%", height: "auto" }}
						></img>
					</div>
				</header>
				<div>{this.props.children}</div>
			</div>
		);
	}
}

export default App;
