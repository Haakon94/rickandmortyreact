import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { NavLink } from "react-router-dom";

// Functional component declared as an arrow function.
const LocationCard = props => {
	let linkButton = null;
	if (props.showLink) {
		linkButton = (
			<NavLink to={{ pathname: "/location/" + props.id }} className="nav-link">
				View location
			</NavLink>
		);
	}
	return (
		<div className="col-md-6">
			<div className="card h-100 LocationCard">
				<div className="card-img"></div>
				<div className="card-body">
					<h4 className="card-title">{props.name}</h4>
					<ul className="list-group list-group-flush">
						<li className="list-group-item">Type: {props.type}</li>
						<li className="list-group-item">Dimension: {props.dimension}</li>
						<li className="list-group-item">{linkButton}</li>
					</ul>
				</div>
			</div>
		</div>
	);
};

export default LocationCard;
