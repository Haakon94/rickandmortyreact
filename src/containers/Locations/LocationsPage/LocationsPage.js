import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import LocationCard from "../../../components/LocationCard/LocationCard";
import SearchBar from "../../SearchBar/SearchBar";

const charHeaderStyle = {
	paddingTop: "1em",
	paddingBottom: "1em"
};

class LocationsPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			locationCards: [],
			loading: <div className="spinner-grow text-muted"></div>
		};
	}

	getData() {
		this.setState({ loading: <div className="spinner-grow text-muted"></div> });
		fetch("https://rickandmortyapi.com/api/location/")
			.then(response => response.json())
			.then(data => {
				if (data.results) {
					this.setState({ locationCards: data.results, loading: false });
				} else if (data.error) {
					this.setState({ loading: <p>Something went wrong...</p> });
				}
			});
	}

	searchForLocation = inputValue => {
		this.setState({ loading: <div className="spinner-grow text-muted"></div> });
		fetch("https://rickandmortyapi.com/api/location/?name=" + inputValue)
			.then(response => response.json())
			.then(data => {
				if (data.results) {
					this.setState({ locationCards: data.results, loading: false });
				} else if (data.error) {
					this.setState({
						locationCards: [],
						loading: <p>No matches for this search, or something went wrong</p>
					});
				}
			});
	};

	componentDidMount() {
		this.getData();
	}

	render() {
		let locations = null;
		if (this.state.locationCards.length > 0) {
			locations = this.state.locationCards.map(location => (
				<LocationCard key={location.id} {...location} showLink={true}></LocationCard>
			));
		}

		return (
			<React.Fragment>
				<div className="container">
					<h2 style={charHeaderStyle}>Locations</h2>
					<SearchBar
						onClick={inputValue => {
							this.searchForLocation(inputValue);
						}}
					/>
					<br />
					<br />
				</div>
				<div className="container">
					{this.state.loading}
					<div className="row">{locations}</div>
				</div>
			</React.Fragment>
		);
	}
}

export default LocationsPage;
