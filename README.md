**Experis Academy, Norway**

# Noroff task Rick & Morty with React

This task uses the open rick & morty api and displays characters and locations using React.

# Hosting

Running version of the program is hosted on Heroku, and can be found [here](https://rickmorty-react.herokuapp.com/).

If running the program locally, use these shell-commands:

## Install

```bash
$ npm install
```

## Usage
