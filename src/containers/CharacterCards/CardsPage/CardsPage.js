import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import CharacterCard from "../../../components/CharacterCard/CharacterCard";
import SearchBar from "../../SearchBar/SearchBar";

const charHeaderStyle = {
	paddingTop: "1em",
	paddingBottom: "1em"
};

// Stateful component
class CardsPage extends React.Component {
	// constructor which sets state with empty arrays as properties
	constructor(props) {
		super(props);
		this.state = {
			rickmorty: [],
			characterCards: [],
			loading: <div className="spinner-grow text-muted"></div>
		};
	}

	// Get data from rick&morty api and use set state to fill characterCards with data results
	getData() {
		this.setState({ loading: <div className="spinner-grow text-muted"></div> });
		fetch("https://rickandmortyapi.com/api/character/")
			.then(response => response.json())
			.then(data => {
				if (data.results) {
					this.setState({ characterCards: data.results, loading: false });
				} else if (data.error) {
					this.setState({ loading: <p>Something went wrong...</p> });
				}
			});
	}

	searchForCharacter = inputValue => {
		this.setState({ loading: <div className="spinner-grow text-muted"></div> });
		fetch("https://rickandmortyapi.com/api/character/?name=" + inputValue)
			.then(response => response.json())
			.then(data => {
				// this.setState({characterCards: data.results, loading: false})
				if (data.results) {
					this.setState({ characterCards: data.results, loading: false });
				} else if (data.error) {
					this.setState({
						characterCards: [],
						loading: <p>No matches for this search, or something went wrong</p>
					});
				}
			});
	};

	componentDidMount() {
		this.getData();
	}

	render() {
		let characters = null;
		if (this.state.characterCards.length > 0) {
			characters = this.state.characterCards.map(character => (
				<CharacterCard key={character.id} {...character} showLink={true}></CharacterCard>
			));
		}

		return (
			<React.Fragment>
				<div className="container">
					<h2 style={charHeaderStyle}>Characters</h2>
					<br />
					<SearchBar
						onClick={inputValue => {
							this.searchForCharacter(inputValue);
						}}
					/>
					<br />
					<br />
				</div>
				<div className="container">
					{this.state.loading}
					<div className="row">{characters}</div>
				</div>
			</React.Fragment>
		);
	}
}

export default CardsPage;
