import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { NavLink } from "react-router-dom";

// Functional component declared as an arrow function.
const CharacterCard = props => {
	let linkButton = null;
	if (props.showLink) {
		linkButton = (
			<NavLink to={{ pathname: "/character/" + props.id }} className="nav-link">
				View character
			</NavLink>
		);
	}
	return (
		<div className="col-md-4">
			<div className="card h-100 CharacterCard">
				<div className="card-img">
					<img src={props.image} alt={props.name} className="card-img-top" />
				</div>
				<div className="card-body">
					<h4 className="card-title">{props.name}</h4>
					<ul className="list-group list-group-flush">
						<li className="list-group-item">Species: {props.species}</li>
						<li className="list-group-item">Status: {props.status}</li>
						<li className="list-group-item">Gender: {props.gender}</li>
						<li className="list-group-item">Location: {props.location.name}</li>
						<li className="list-group-item">Origin: {props.origin.name}</li>
						<li className="list-group-item">{linkButton}</li>
					</ul>
				</div>
			</div>
		</div>
	);
};

export default CharacterCard;
